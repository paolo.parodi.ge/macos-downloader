#!/bin/bash

parameters="${1}${2}${3}${4}${5}${6}${7}${8}${9}"

Escape_Variables()
{
	text_progress="\033[38;5;113m"
	text_success="\033[38;5;113m"
	text_warning="\033[38;5;221m"
	text_error="\033[38;5;203m"
	text_message="\033[38;5;75m"

	text_bold="\033[1m"
	text_faint="\033[2m"
	text_italic="\033[3m"
	text_underline="\033[4m"

	erase_style="\033[0m"
	erase_line="\033[0K"

	move_up="\033[1A"
	move_down="\033[1B"
	move_foward="\033[1C"
	move_backward="\033[1D"
}

Parameter_Variables()
{
	if [[ $parameters == *"-v"* || $parameters == *"-verbose"* ]]; then
		verbose="1"
		set -x
	fi

	if [[ $parameters == *"-l"* || $parameters == *"-local"* ]]; then
		local="1"
	fi

	if [[ $parameters == *"-sc"* || $parameters == *"-search-catalogue"* ]]; then
		searchcatalogue="1"
		Search_Catalogue
		Print_Catalogue
		End
	fi
}

Real_Path()
{
	[[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#.}"
}

Path_Variables()
{
	script_path="${0}"
	directory_path="$(Real_Path "${0%/*}")"

	resources_path="$directory_path/resources"

	if [[ -d /usr/local/opt/macos-downloader ]]; then
		resources_path="/usr/local/opt/macos-downloader"
	fi
}

Input_Off()
{
	stty -echo
}

Input_On()
{
	stty echo
}

Output_Off()
{
	if [[ $verbose == "1" ]]; then
		"$@"
	else
		"$@" &>/dev/null
	fi
}

Check_Environment()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system environment."${erase_style}

		if [ -d /Install\ *.app ]; then
			environment="installer"
		else
			environment="system"
		fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked system environment."${erase_style}
}

Check_Resources()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking for resources."${erase_style}

	if [[ -d "$resources_path" ]]; then
		resources_check="passed"
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Resources check passed."${erase_style}
	else
		resources_check="failed"
		echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Resources check failed."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with the required resources."${erase_style}

		Input_On
		exit
	fi
}

Input_Folder()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What save folder would you like to use?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input a save folder path."${erase_style}

	Input_On
	read -e -p "$(date "+%b %d %H:%M:%S") / " save_folder
	Input_Off
}

Check_Write()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking for write permissions on save folder."${erase_style}

	if [[ -w "$save_folder" ]]; then
		write_check="passed"
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Write permissions check passed."${erase_style}
	else
		root_check="failed"
		echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Write permissions check failed."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with a writable save folder."${erase_style}

		Input_On
		exit
	fi
}

Input_Version()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What operation would you like to run?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input an operation number."${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     1 - $l_installer_version Lion"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     2 - $ml_installer_version Mountain Lion"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     3 - $y_installer_version Yosemite"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     4 - $ec_installer_version El Capitan"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     5 - $s_installer_version Sierra"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     6 - $hs_installer_version High Sierra"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     7 - $m_installer_version Mojave"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     8 - $c_installer_version Catalina"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     9 - $bs_installer_version Big Sur"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     10 - $mo_installer_version Monterey"${erase_style}


	Input_On
	read -e -p "$(date "+%b %d %H:%M:%S") / " operation_version
	Input_Off

	
	if [[ $operation_version == "1" ]]; then
		installer_choice="l"
	fi

	if [[ $operation_version == "2" ]]; then
		installer_choice="ml"
	fi

	if [[ $operation_version == "3" ]]; then
		installer_choice="y"
	fi

	if [[ $operation_version == "4" ]]; then
		installer_choice="ec"
	fi

	if [[ $operation_version == "5" ]]; then
		installer_choice="s"
	fi

	if [[ $operation_version == "6" ]]; then
		installer_choice="hs"
	fi

	if [[ $operation_version == "7" ]]; then
		installer_choice="m"
	fi

	if [[ $operation_version == "8" ]]; then
		installer_choice="c"
	fi

	if [[ $operation_version == "9" ]]; then
		installer_choice="bs"
	fi

	if [[ $operation_version == "10" ]]; then
		installer_choice="mo"
	fi

	installer_url="${installer_choice}_installer_url"
	installer_name="${installer_choice}_installer_name"
	installer_version="${installer_choice}_installer_version"

	if [[ $operation_version == [1-5] ]]; then
		Import_Second_Catalogue
		Download_Installer_2
		Prepare_Installer_2
		Import_Second_Catalogue
	fi

	if [[ $operation_version == [6-8] ]]; then
		Import_Second_Catalogue
		Download_Installer
		Prepare_Installer
		Import_Second_Catalogue
	fi

	if [[ $operation_version == "9" || $operation_version == "10" ]]; then
		Import_Second_Catalogue
		Download_Installer_3
		Prepare_Installer_3
		Import_Second_Catalogue
	fi
}

Import_Catalogue()
{
	l_installer_url="https://updates.cdn-apple.com/2021/macos/041-7683-20210614-E610947E-C7CE-46EB-8860-D26D71F0D3EA/InstallMacOSX.dmg"
	l_installer_name="Install Mac OS X Lion"
	l_installer_version="10.7.5"

	ml_installer_url="https://updates.cdn-apple.com/2021/macos/031-0627-20210614-90D11F33-1A65-42DD-BBEA-E1D9F43A6B3F/InstallMacOSX.dmg"
	ml_installer_name="Install OS X Mountain Lion"
	ml_installer_version="10.8.5"

	y_installer_url="http://updates-http.cdn-apple.com/2019/cert/061-41343-20191023-02465f92-3ab5-4c92-bfe2-b725447a070d/InstallMacOSX.dmg"
	y_installer_name="Install OS X Yosemite"
	y_installer_version="10.10.5"

	ec_installer_url="http://updates-http.cdn-apple.com/2019/cert/061-41424-20191024-218af9ec-cf50-4516-9011-228c78eda3d2/InstallMacOSX.dmg"
	ec_installer_name="Install OS X El Capitan"
	ec_installer_version="10.11.6"

	s_installer_url="http://updates-http.cdn-apple.com/2019/cert/061-39476-20191023-48f365f4-0015-4c41-9f44-39d3d2aca067/InstallOS.dmg"
	s_installer_name="Install macOS Sierra"
	s_installer_version="10.12.6"

	hs_installer_url="06/50/041-91758-A_M8T44LH2AW/b5r4og05fhbgatve4agwy4kgkzv07mdid9"
	hs_installer_name="Install macOS High Sierra"
	hs_installer_version="10.13.6"

	m_installer_url="17/32/061-26589-A_8GJTCGY9PC/25fhcu905eta7wau7aoafu8rvdm7k1j4el"
	m_installer_name="Install macOS Mojave"
	m_installer_version="10.14.6"

	c_installer_url="20/55/001-51042-A_2EJTJOSUC2/rsvf13iphg5lvcqcysqcarv8cvddq8igek"
	c_installer_name="Install macOS Catalina"
	c_installer_version="10.15.7"
}

Search_Catalogue()
{
	for entry in $(curl -L -s https://swscan.apple.com/content/catalogs/others/index-10.16seed-10.16-10.15-10.14-10.13-10.12-10.11-10.10-10.9-mountainlion-lion-snowleopard-leopard.merged-1.sucatalog | grep "InstallAssistant.pkg<" | sed 's/.*<string>//' | sed 's/\/InstallAssistant.*//'); do
		info=$(curl -L -s "$entry/Info.plist")
		version="$(echo "$info" | grep -A1 OSVersion | grep string | sed 's/.*<string>//' | sed 's/<\/string>//')"
		isseed="$(echo "$info" | grep -A1 IsSeed | grep string | sed 's/.*<string>//' | sed 's/<\/string>//')"

		if [[ "$version" == 11* ]]; then
			if [[ "$isseed" == "NO" ]]; then
				bs_installer_urls="$bs_installer_urls
$entry/InstallAssistant.pkg"
				bs_installer_versions="$bs_installer_versions
$version"
			fi
		fi

		if [[ "$version" == 12* ]]; then
			if [[ "$isseed" == "NO" ]]; then
				mo_installer_urls="$mo_installer_urls
$entry/InstallAssistant.pkg"
				mo_installer_versions="$mo_installer_versions
$version"
			fi
		fi

		if [[ "$version" == 13* ]]; then
			if [[ "$isseed" == "NO" ]]; then
				v_installer_urls="$v_installer_urls
$entry/InstallAssistant.pkg"
				v_installer_versions="$v_installer_versions
$version"
			else
				v_beta_installer_urls="$v_beta_installer_urls
$entry/InstallAssistant.pkg"
				v_beta_installer_versions="$v_beta_installer_versions
$version"
			fi
		fi
	done

	bs_installer_url="$(echo "$bs_installer_urls" | tail -1)"
	bs_installer_version="$(echo "$bs_installer_versions" | tail -1)"

	bs_installer_name="Install macOS Big Sur"

	mo_installer_url="$(echo "$mo_installer_urls" | tail -1)"
	mo_installer_version="$(echo "$mo_installer_versions" | tail -1)"

	mo_installer_name="Install macOS Monterey"

	v_installer_url="$(echo "$v_installer_urls" | tail -1)"
	v_installer_version="$(echo "$v_installer_versions" | tail -1)"
	v_beta_installer_url="$(echo "$v_beta_installer_urls" | tail -1)"
	v_beta_installer_version="$(echo "$v_beta_installer_versions" | tail -1)"

	if [[ (( "$v_beta_installer_version" > "$v_installer_version" )) ]]; then
		v_beta_installer_option="1"
	else
		v_beta_installer_option="0"
	fi

	v_installer_name="Install macOS Ventura"
	v_beta_installer_name="Install macOS Ventura Beta"
}

Print_Catalogue()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ $bs_installer_version Big Sur - $bs_installer_url"${erase_style}

	if [[ (( "$bs_beta_installer_option" == "1" )) ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ $bs_beta_installer_version Big Sur Beta - $bs_beta_installer_url"${erase_style}
	fi

	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ $mo_installer_version Monterey - $mo_installer_url"${erase_style}

	if [[ (( "$mo_beta_installer_option" == "1" )) ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ $mo_beta_installer_version Monterey Beta - $mo_beta_installer_url"${erase_style}
	fi

	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ $v_installer_version Ventura - $v_installer_url"${erase_style}

	if [[ (( "$v_beta_installer_option" == "1" )) ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ $v_beta_installer_version Ventura Beta - $v_beta_installer_url"${erase_style}
	fi
}

Import_Second_Catalogue()
{
	if [[ -d /tmp/"${!installer_name}" ]]; then
		chmod +x /tmp/"${!installer_name}"/Catalogue.sh
		source /tmp/"${!installer_name}"/Catalogue.sh
	fi
}

Download_Installer()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Downloading installer files."${erase_style}

		if [[ ! -d /tmp/"${!installer_name}" ]]; then
			mkdir /tmp/"${!installer_name}"
		fi

		echo -e "installer_download=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
	
		if [[ ! "$InstallAssistantAuto_pkg" == "1" ]]; then
			curl -L -s -o /tmp/"${!installer_name}"/InstallAssistantAuto.pkg http://swcdn.apple.com/content/downloads/${!installer_url}/InstallAssistantAuto.pkg
			echo -e "InstallAssistantAuto_pkg=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi
	
		if [[ ! "$AppleDiagnostics_chunklist" == "1" ]]; then
			curl -L -s -o /tmp/"${!installer_name}"/AppleDiagnostics.chunklist http://swcdn.apple.com/content/downloads/${!installer_url}/AppleDiagnostics.chunklist
			echo -e "AppleDiagnostics_chunklist=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi
	
		if [[ ! "$AppleDiagnostics_dmg" == "1" ]]; then
			curl -L -s -o /tmp/"${!installer_name}"/AppleDiagnostics.dmg http://swcdn.apple.com/content/downloads/${!installer_url}/AppleDiagnostics.dmg
			echo -e "AppleDiagnostics_dmg=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi
	
		if [[ ! "$BaseSystem_chunklist" == "1" ]]; then
			curl -L -s -o /tmp/"${!installer_name}"/BaseSystem.chunklist http://swcdn.apple.com/content/downloads/${!installer_url}/BaseSystem.chunklist
			echo -e "BaseSystem_chunklist=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi
	
		if [[ ! "$BaseSystem_dmg" == "1" ]]; then
			curl -L -s -o /tmp/"${!installer_name}"/BaseSystem.dmg http://swcdn.apple.com/content/downloads/${!installer_url}/BaseSystem.dmg
			echo -e "BaseSystem_dmg=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi
	
		if [[ ! "$InstallESD_dmg" == "1" ]]; then
			curl -L -s -o /tmp/"${!installer_name}"/InstallESD.dmg http://swcdn.apple.com/content/downloads/${!installer_url}/InstallESDDmg.pkg
			echo -e "InstallESD_dmg=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		echo -e "installer_download=\"2\"" >> /tmp/"${!installer_name}"/Catalogue.sh

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Downloaded installer files."${erase_style}
}

Prepare_Installer()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Preparing installer."${erase_style}

		echo -e "installer_prepare=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh

		cd /tmp/"${!installer_name}"
	
		if [[ ! "$InstallAssistantAuto_pkg" == "2" ]]; then
			chmod +x "$resources_path"/pbzx
			"$resources_path"/pbzx /tmp/"${!installer_name}"/InstallAssistantAuto.pkg | Output_Off cpio -i
			echo -e "InstallAssistantAuto_pkg=\"2\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi


		if [[ ! "$InstallAssistantAuto_pkg" == "3" ]]; then
			mv /tmp/"${!installer_name}"/"${!installer_name}".app "$save_folder"
			echo -e "InstallAssistantAuto_pkg=\"3\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		if [[ ! "$AppleDiagnostics_chunklist" == "3" ]]; then
			mv /tmp/"${!installer_name}"/AppleDiagnostics.chunklist "$save_folder"/"${!installer_name}".app/Contents/SharedSupport
			echo -e "AppleDiagnostics_chunklist=\"3\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		if [[ ! "$AppleDiagnostics_dmg" == "3" ]]; then
			mv /tmp/"${!installer_name}"/AppleDiagnostics.dmg "$save_folder"/"${!installer_name}".app/Contents/SharedSupport
			echo -e "AppleDiagnostics_dmg=\"3\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		if [[ ! "$BaseSystem_chunklist" == "3" ]]; then
			mv /tmp/"${!installer_name}"/BaseSystem.chunklist "$save_folder"/"${!installer_name}".app/Contents/SharedSupport
			echo -e "BaseSystem_chunklist=\"3\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		if [[ ! "$BaseSystem_dmg" == "3" ]]; then
			mv /tmp/"${!installer_name}"/BaseSystem.dmg "$save_folder"/"${!installer_name}".app/Contents/SharedSupport
			echo -e "BaseSystem_dmg=\"3\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		if [[ ! "$InstallESD_dmg" == "3" ]]; then
			mv /tmp/"${!installer_name}"/InstallESD.dmg "$save_folder"/"${!installer_name}".app/Contents/SharedSupport
			echo -e "InstallESD_dmg=\"3\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		echo -e "installer_prepare=\"2\"" >> /tmp/"${!installer_name}"/Catalogue.sh

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Prepared installer."${erase_style}
}

Download_Installer_2()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Downloading installer files."${erase_style}

		if [[ ! -d /tmp/"${!installer_name}" ]]; then
			mkdir /tmp/"${!installer_name}"
		fi

		echo -e "installer_download=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
	
		if [[ ! "$Install_dmg" == "1" ]]; then
			curl -L -s -o /tmp/"${!installer_name}"/"${!installer_name}".dmg ${!installer_url}
			echo -e "Install_dmg=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		echo -e "installer_download=\"2\"" >> /tmp/"${!installer_name}"/Catalogue.sh

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Downloaded installer files."${erase_style}
}

Prepare_Installer_2()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Preparing installer."${erase_style}

		echo -e "installer_prepare=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		Output_Off hdiutil attach /tmp/"${!installer_name}"/"${!installer_name}".dmg -mountpoint /tmp/"${!installer_name}"_dmg -nobrowse

		installer_pkg="$(ls /tmp/"${!installer_name}"_dmg)"
		installer_pkg_partial="${installer_pkg%.*}"

		if [[ ! "$Install_pkg" == "1" ]]; then
			pkgutil --expand /tmp/"${!installer_name}"_dmg/"${installer_pkg}" /tmp/"${!installer_name}"/"${installer_pkg_partial}"
			echo -e "Install_pkg=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		if [[ ! "$Install_pkg" == "2" ]]; then
			tar -xf /tmp/"${!installer_name}"/"${installer_pkg_partial}"/"${installer_pkg}"/Payload -C "$save_folder"
			echo -e "Install_pkg=\"2\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		if [[ ! "$InstallESD_dmg" == "1" ]]; then
			cp /tmp/"${!installer_name}"_dmg/"${installer_pkg}" "$save_folder"/"${!installer_name}".app/Contents/SharedSupport/InstallESD.dmg
			echo -e "InstallESD_dmg=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		Output_Off hdiutil detach /tmp/"${!installer_name}"_dmg
		echo -e "installer_prepare=\"2\"" >> /tmp/"${!installer_name}"/Catalogue.sh

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Prepared installer."${erase_style}
}

Download_Installer_3()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Downloading installer files."${erase_style}

		if [[ ! -d /tmp/"${!installer_name}" ]]; then
			mkdir /tmp/"${!installer_name}"
		fi

		echo -e "installer_download=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
	
		if [[ ! "$Install_pkg" == "1" ]]; then
			curl -L -s -o /tmp/"${!installer_name}"/"${!installer_name}".pkg ${!installer_url}
			echo -e "Install_pkg=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		echo -e "installer_download=\"2\"" >> /tmp/"${!installer_name}"/Catalogue.sh

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Downloaded installer files."${erase_style}
}

Prepare_Installer_3()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Preparing installer."${erase_style}

		echo -e "installer_prepare=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		if [[ ! "$Install_pkg" == "1" ]]; then
			Output_Off pkgutil --expand /tmp/"${!installer_name}"/"${!installer_name}".pkg /tmp/"${!installer_name}"/"${!installer_name}"
			echo -e "Install_pkg=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		if [[ ! "$Install_pkg" == "2" ]]; then
			tar -xf /tmp/"${!installer_name}"/"${!installer_name}"/Payload -C /tmp/"${!installer_name}" Applications/"${!installer_name}".app
			mv /tmp/"${!installer_name}"/Applications/"${!installer_name}".app "$save_folder"
			echo -e "Install_pkg=\"2\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		if [[ ! "$SharedSupport_dmg" == "1" ]]; then
			mkdir -p "$save_folder"/"${!installer_name}".app/Contents/SharedSupport
			mv /tmp/"${!installer_name}"/"${!installer_name}".pkg "$save_folder"/"${!installer_name}".app/Contents/SharedSupport/SharedSupport.dmg
			echo -e "SharedSupport_dmg=\"1\"" >> /tmp/"${!installer_name}"/Catalogue.sh
		fi

		echo -e "installer_prepare=\"2\"" >> /tmp/"${!installer_name}"/Catalogue.sh

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Prepared installer."${erase_style}
}

End()
{
	if [[ ! $searchcatalogue == "1" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing temporary files."${erase_style}

			if [[ "$installer_prepare" == "2" ]]; then
				Output_Off rm -R /tmp/"${!installer_name}"
			fi

			if [[ "$update_prepare" == "2" ]]; then
				Output_Off rm -R /tmp/"${update_name}${update_version}"
			fi

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed temporary files."${erase_style}
	fi

	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Thank you for using macOS Downloader."${erase_style}

	Input_On
	exit
}

Input_Off
Escape_Variables
Parameter_Variables
Path_Variables
Check_Environment
Check_Resources
Input_Folder
Check_Write
Import_Catalogue
Search_Catalogue
Input_Version
End